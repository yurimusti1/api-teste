import { User } from '../src/model/User';

describe('Should be test User controller', () => {
  test('should create a new User', () => {
    const user = new User('Yuri');
    expect(user.name).toBe('Yuri');
  });
});
