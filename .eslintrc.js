'use strict';

module.exports = {
  root: true,

  plugins: ['@typescript-eslint'],
  parser: '@typescript-eslint/parser',

  env: {
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },

  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    tsconfigRootDir: __dirname,
    project: ['./tsconfig.json'],
  },

  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'prettier',
  ],

  rules: {
    'arrow-body-style': ['none'],
    'prefer-arrow-callback': ['none'],
    '@typescript-eslint/array-type': ['none'],
    '@typescript-eslint/no-use-before-define': ['none'],
    '@typescript-eslint/restrict-template-expressions': ['none'],
    '@typescript-eslint/no-unsafe-return': ['none'],
    '@typescript-eslint/no-unsafe-assignment': ['none'],
    '@typescript-eslint/no-unused-vars': ['none'],
    '@typescript-eslint/explicit-module-boundary-types': ['none'],
    '@typescript-eslint/no-explicit-any': ['none'],
    'no-unsafe-finally': ['none'],
    '@typescript-eslint/no-unsafe-member-access': ['none'],
    '@typescript-eslint/no-unsafe-call': '',
    '@typescript-eslint/no-unsafe-return': ['none'],
  },
};
