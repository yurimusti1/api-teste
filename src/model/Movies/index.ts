interface Category {
  movie: boolean;
  serie: boolean;
}

export class Movies {
  id: number | undefined;
  name: string;
  description: string;
  category: Category;
  limit: number;
  lastChange: Date;
  createdAt: Date;

  constructor(
    name: string,
    description: string,
    category: Category,
    limit: number,
  ) {
    this.name = name;
    this.description = description;
    this.category = category;
    this.limit = limit;
    this.createdAt = new Date();
    this.lastChange = new Date();
  }
}
