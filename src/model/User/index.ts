import { Books } from '../Books';
import { Movies } from '../Movies';
import { Games } from '../Games';

export class User {
  id: string | undefined;
  name: string;
  listRentBooks: Books[];
  listRentGame: Games[];
  listRentMovie: Movies[];
  lastChange: Date;
  createdAt: Date;

  constructor(name: string) {
    this.name = name;
    this.listRentBooks = [];
    this.listRentMovie = [];
    this.listRentGame = [];
    this.lastChange = new Date();
    this.createdAt = new Date();
  }
}
