

interface Category {
  videoGame: boolean;
  boardGame: boolean;
}

export class Games {
  id: number | undefined;
  name: string;
  description: string;
  category: Category;
  limit: number;
  lastChange: Date;
  createdAt: Date;

  constructor(
    name: string,
    description: string,
    category: Category,
    limit: number,
  ) {
    this.name = name;
    this.description = description;
    this.category = category;
    this.limit = limit;
    this.lastChange = new Date();
    this.createdAt = new Date();
  }
}
