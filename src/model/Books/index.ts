interface Category {
  book: boolean;
  audioBook: boolean;
}

export class Books {
  id: number | undefined;
  name: string;
  author: string;
  description: string;
  category: Category;
  limit: number;
  lastChange: Date;
  createdAt: Date;

  constructor(
    name: string,
    author: string,
    description: string,
    category: Category,
    limit: number,
  ) {
    this.name = name;
    this.author = author;
    this.description = description;
    this.category = category;
    this.limit = limit;
    this.lastChange = new Date();
    this.createdAt = new Date();
  }
}
