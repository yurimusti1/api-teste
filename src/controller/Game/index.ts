import { ObjectID } from 'mongodb';
import { Games } from '../../model/Games';

export const createGame = async (game: Games, db: any) => {
  try {
    await db?.collection('game').insertOne({ ...game });
    return true;
  } catch (error) {
    return false;
  }
};

export const rentGames = async (idUser: string, idGame: string, db: any) => {
    const user = await db.collection('user').find({}).toArray();
    const game = await db.collection('game').find({}).toArray();
  
    const currUser = user.filter((e: any) => e._id == idUser);
    const currGame = game
      .filter((e: any) => e._id == idGame)
      .map((e: any) => ({ ...e, _id: e._id.toString() }));

      
  
    if (
      currUser[0].listRentGame.filter((e: any) => e == currGame[0]._id.toString()).length >
      0
    ) {
      return {
        message: `This game has been rented on ${currUser[0].lastChange}`,
      };
    } 

    if (currUser !== undefined && currGame !== undefined)
      if (currGame[0].limit > 0) {
        const newUser = {
          ...user[0],
          _id: user[0]._id,
          listRentGame: [...user[0].listRentGame, currGame[0]._id],
        };
  
        const newGame = {
          ...game[0],
          _id: game[0]._id,
          limit: game[0].limit - 1,
        };
  
        await db.collection('game').updateOne(
          { _id: new ObjectID(newGame._id) },
          {
            $set: {
              lastChange: new Date(),
              limit: game[0].limit - 1,
            },
          },
          { upsert: true },
        );
        await db.collection('user').updateOne(
          { _id: new ObjectID(newUser._id) },
          {
            $set: {
              lastChange: new Date(),
              listRentGame: [...newUser.listRentGame],
            },
          },
          { upsert: true },
        );
        return { message: 'Game rented' };
      } else {
        return { message: 'Dont have any game to rent' };
      }
  };