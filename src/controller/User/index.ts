/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { ObjectID } from 'mongodb';
import { User } from '../../model/User';

export const getUser = async (userId: string, db: any) => {
  return await db?.collection('user')?.find({ _id: new ObjectID(userId) });
};

export const getAllUsers = async (db: any) =>
  await db?.collection('user')?.find().toArray();

export const createUser = async (user: User, db: any) => {
  try {
    await db?.collection('user').insertOne({ ...user });
    return true;
  } catch (error) {
    return false;
  }
};
