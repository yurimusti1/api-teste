import { ObjectID } from 'mongodb';
import { Movies } from '../../model/Movies';

export const createMovie = async (movie: Movies, db: any) => {
  try {
    await db?.collection('movie').insertOne({ ...movie });
    return true;
  } catch (error) {
    return false;
  }
};

export const rentMovie = async (idUser: string, idMovie: string, db: any) => {
  const user = await db.collection('user').find({}).toArray();
  const movie = await db.collection('movie').find({}).toArray();

  const currUser = user.filter((e: any) => e._id == idUser);
  const currMovie = movie
    .filter((e: any) => e._id == idMovie)
    .map((e: any) => ({ ...e, _id: e._id.toString() }));

  if (
    currUser[0].listRentMovie.filter(
      (e: any) => e === currMovie[0]._id.toString(),
    ).length > 0
  ) {
    return {
      message: `This movie has been rented on ${currUser[0].lastChange}`,
    };
  }
  if (currUser !== undefined && currMovie !== undefined)
    if (currMovie[0].limit > 0) {
      const newUser = {
        ...user[0],
        _id: user[0]._id,
        listRentMovie: [...user[0].listRentMovie, currMovie[0]._id],
      };

      const newGame = {
        ...movie[0],
        _id: movie[0]._id,
        limit: movie[0].limit - 1,
      };

      await db.collection('movie').updateOne(
        { _id: new ObjectID(newGame._id) },
        {
          $set: {
            lastChange: new Date(),
            limit: movie[0].limit - 1,
          },
        },
        { upsert: true },
      );
      await db.collection('user').updateOne(
        { _id: new ObjectID(newUser._id) },
        {
          $set: {
            lastChange: new Date(),
            listRentMovie: [...newUser.listRentMovie],
          },
        },
        { upsert: true },
      );
      return { message: 'Movie rented' };
    } else {
      return { message: 'Dont have any movie to rent' };
    }
};

