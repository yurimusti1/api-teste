import { ObjectID } from 'mongodb';
import { Books } from '../../model/Books';

export const createBooks = async (book: Books, db: any) => {
  try {
    await db?.collection('book').insertOne({ ...book });
    return true;
  } catch (error) {
    return false;
  }
};

export const rentBook = async (idUser: string, idMovie: string, db: any) => {
  const user = await db.collection('user').find({}).toArray();
  const game = await db.collection('book').find({}).toArray();

  const currUser = user.filter((e: any) => e._id == idUser);
  const currBook = game
    .filter((e: any) => e._id == idMovie)
    .map((e: any) => ({ ...e, _id: e._id.toString() }));

  if (
    currUser[0].listRentBooks.filter(
      (e: any) => e === currBook[0]._id.toString(),
    ).length > 0
  ) {
    return {
      message: `This book has been rented on ${currUser[0].lastChange}`,
    };
  }
  if (currUser !== undefined && currBook !== undefined)
    if (currBook[0].limit > 0) {
      const newUser = {
        ...user[0],
        _id: user[0]._id,
        listRentBooks: [...user[0].listRentBooks, currBook[0]._id],
      };

      const newGame = {
        ...game[0],
        _id: game[0]._id,
        limit: game[0].limit - 1,
      };

      await db.collection('book').updateOne(
        { _id: new ObjectID(newGame._id) },
        {
          $set: {
            lastChange: new Date(),
            limit: game[0].limit - 1,
          },
        },
        { upsert: true },
      );
      await db.collection('user').updateOne(
        { _id: new ObjectID(newUser._id) },
        {
          $set: {
            lastChange: new Date(),
            listRentBooks: [...newUser.listRentBooks],
          },
        },
        { upsert: true },
      );
      return { message: 'Book rented' };
    } else {
      return { message: 'Dont have any book to rent' };
    }
};
