/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
require('dotenv').config();

import bodyParser = require('body-parser');

import * as express from 'express';
import * as Mongodb from 'mongodb';

import { Books } from './model/Books';
import { Games } from './model/Games';
import { Movies } from './model/Movies';
import { User } from './model/User';

import {
  Book as BookController,
  Game as GameController,
  User as UserController,
  Movie as MovieController,
} from './controller';

const MongoClient: any = Mongodb.MongoClient;
const client = new MongoClient(`${process.env.MONGO_URL}`, {
  useUnifiedTopology: true,
});

client.connect((err: any, client: any) => {
  console.log(
    err !== undefined ? 'Nao conectado ao BD' : 'Conectado no banco de dados.',
  );
  const app = express();
  app.use(bodyParser.json());

  const db = client.db('database_test');

  app.get('/', (req, res) => {
    res.send({ status: 'online' });
  });

  app.post('/createBook', async (req: any, res: any) => {
    if (req.body === undefined) req.send({ message: 'Book dont create' });
    const { name, author, description, category, limit } = req.body;
    const book = new Books(name, author, description, category, limit);

    if ((await BookController.createBooks(book, db)) === true) {
      res.send({ message: 'User created.' });
    } else {
      res.send({ message: 'User dont create' });
    }

    await client.close();
  });

  app.post('/createGame', async (req: any, res: any) => {
    if (req.body === undefined) req.send({ message: 'Game dont create' });
    const { name, description, category, limit } = req.body;

    const game = new Games(name, description, category, limit);

    if ((await GameController.createGame(game, db)) === true) {
      res.send({ message: 'Game created.' });
    } else {
      res.send({ message: 'Game dont create' });
    }

    await client.close();
  });

  app.post('/createMovie', async (req: any, res: any) => {
    if (req.body === undefined) req.send({ message: 'Game dont create' });
    const { name, description, category, limit } = req.body;

    const movie = new Movies(name, description, category, limit);

    if ((await MovieController.createMovie(movie, db)) === true) {
      res.send({ message: 'Movie created.' });
    } else {
      res.send({ message: 'Movie dont create' });
    }

    await client.close();
  });

  app.post('/createUser', async (req: any, res: any) => {
    if (req.body.name === undefined)
      res.send({ message: 'User dont create (Undefined)' });

    const user = new User(req.body.name);
    if ((await UserController.createUser(user, db)) === true) {
      res.send({ message: 'User created.' });
    } else {
      res.send({ message: 'User dont create' });
    }

    await client.close();
  });

  app.get('/getAllUsers', async (req: any, res: any) => {
    res.send({ data: await UserController.getAllUsers(db) });
    await client.close();
  });

  app.post('/rentGame', async (req: any, res: any) => {
    if (req.body !== undefined) {
      const { idUser, idGame } = req.body;
      const data = await GameController.rentGames(idUser, idGame, db);
      res.send(data);
      // await client.close();
    } else {
      res.send({ message: 'Sorry' });
    }
  });

  app.post('/rentBook', async (req: any, res: any) => {
    if (req.body !== undefined) {
      const { idUser, idBook } = req.body;
      const data = await BookController.rentBook(idUser, idBook, db);
      res.send(data);
      // await client.close();
    } else {
      res.send({ message: 'Sorry' });
    }
  });

  app.post('/rentMovie', async (req: any, res: any) => {
    if (req.body !== undefined) {
      const { idUser, idMovie } = req.body;
      const data = await MovieController.rentMovie(idUser, idMovie, db);
      res.send(data);
      // await client.close();
    } else {
      res.send({ message: 'Sorry' });
    }
  });

  app.listen(3000, () => {
    console.log(`Example app listening on port ${3000}`);
  });
});
